//
//  RequestConstants.swift
//  challenge
//
//  Created by Enzo Digiano on 17/08/2020.
//

import Foundation

public enum HTTP_RESPONSE_TYPE: Error, Equatable {
    case HTTP_SUCCESS
    case HTTP_FAILURE(String)
    case HTTP_FORBIDEN
    case HTTP_BAD_REQUEST
    case HTTP_SERVER_ERROR
    case HTTP_UNAUTHORIZED
    case HTTP_NOT_FOUND
    case NO_STATUS_CODE
}
