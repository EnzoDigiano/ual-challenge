//
//  Meal.swift
//  challenge
//
//  Created by Enzo Digiano on 17/08/2020.
//

import Foundation

class Meal {
    var id: String = ""
    var pictureURL: String = ""
    var name: String = ""
    var category: String = ""
    var instructions: String = ""
    var videoURL: String = ""
    var ingredients: [String] = []
}

extension MealsMeal {
    
    /// Parse Response to `Meal` object
    /// - Returns: `Meal` object
    func toMeal() -> Meal{
        let result = Meal()
        result.id = self.idMeal ?? ""
        result.pictureURL = self.strMealThumb ?? ""
        result.name = self.strMeal ?? ""
        result.category = self.strCategory ?? ""
        result.instructions = self.strInstructions ?? ""
        result.videoURL = self.strYoutube ?? ""
        if let ingredient = self.strIngredient1, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient2, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient3, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient4, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient5, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient6, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient7, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient8, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient9, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient10, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient11, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient12, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient13, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient14, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient15, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient16, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient17, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient18, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient19, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        if let ingredient = self.strIngredient20, ingredient != "" {
            result.ingredients.append(ingredient)
        }
        return result
    }
}
