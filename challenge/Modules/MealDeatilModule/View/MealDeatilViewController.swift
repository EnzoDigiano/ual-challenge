//
//  MealDeatilViewController.swift
//  challenge
//
//  Created by Enzo Digiano on 17/08/2020.
//

import UIKit
import Kingfisher
import AVFoundation

class MealDeatilViewController: UIViewController, ViewInterface {

    var presenter: MealDeatilModulePresenterViewInterface!
    var meal: Meal!
    
    @IBOutlet weak var mealImage: UIImageView!
    @IBOutlet weak var mealInstructions: UILabel!
    @IBOutlet weak var ingredientsStackView: UIStackView!
    @IBOutlet weak var videoContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configure()
    }
     
    internal func configure(){
        self.navigationController?.navigationBar.topItem?.title = "Back"
        self.mealInstructions.text = meal.instructions
        getImage(with: meal.pictureURL)
        fillIngredients(ingredients: meal.ingredients)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        getVideo(with: meal.videoURL)
    }
    
    internal func getVideo(with url: String){
        let videoURL = URL(string: url)
        let player = AVPlayer(url: videoURL!)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.videoContainer.bounds
        self.videoContainer.layer.addSublayer(playerLayer)
        player.play()
    }
    
    internal func getImage(with path: String) {
        if let imageURL = URL(string: path) {
             mealImage.kf.setImage(with: .network(imageURL), options: [.transition(.fade(0.2))]) { result in
                 switch result {
                     case .success( _):
                         print("Sucessfully get image")
                     case .failure( _):
                         print("Fail to get image")
                 }
             }
         } else {
            print("Fail to make image URL")
         }
    }
    
    internal func fillIngredients(ingredients: [String]){
        for ingredient in ingredients{
            let label = UILabel()
            label.text = " \(ingredient) "
            label.backgroundColor = .systemTeal
            ingredientsStackView.addArrangedSubview(label)
        }
    }
}

// MARK: - Presenter
extension MealDeatilViewController: MealDeatilModuleViewPresenterInterface {

}
