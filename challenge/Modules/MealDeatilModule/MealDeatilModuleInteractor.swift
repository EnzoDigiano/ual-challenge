//
//  MealDeatilModuleInteractor.swift
//  MealDeatilModule
//
//  Created by Enzo Digiano on 17/8/20.
//

import Foundation

final class MealDeatilModuleInteractor: InteractorInterface {

    weak var presenter: MealDeatilModulePresenterInteractorInterface!
}

// MARK: - Presenter
extension MealDeatilModuleInteractor: MealDeatilModuleInteractorPresenterInterface {

}
