//
//  MealDeatilModuleRouter.swift
//  MealDeatilModule
//
//  Created by Enzo Digiano on 17/8/20.
//

import Foundation
import UIKit

final class MealDeatilModuleRouter: RouterInterface {

    weak var presenter: MealDeatilModulePresenterRouterInterface!
    weak var viewController: UIViewController?
}

// MARK: - Presenter
extension MealDeatilModuleRouter: MealDeatilModuleRouterPresenterInterface {

}
