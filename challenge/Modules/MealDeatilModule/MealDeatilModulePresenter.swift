//
//  MealDeatilModulePresenter.swift
//  MealDeatilModule
//
//  Created by Enzo Digiano on 17/8/20.
//

import Foundation

final class MealDeatilModulePresenter: PresenterInterface {

    var router: MealDeatilModuleRouterPresenterInterface!
    var interactor: MealDeatilModuleInteractorPresenterInterface!
    weak var view: MealDeatilModuleViewPresenterInterface!

}

// MARK: - Router
extension MealDeatilModulePresenter: MealDeatilModulePresenterRouterInterface {

}

// MARK: - Interactor
extension MealDeatilModulePresenter: MealDeatilModulePresenterInteractorInterface {

}

// MARK: - View
extension MealDeatilModulePresenter: MealDeatilModulePresenterViewInterface {

}
