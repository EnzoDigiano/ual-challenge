//
//  MealDeatilModuleModule.swift
//  MealDeatilModule
//
//  Created by Enzo Digiano on 17/8/20.
//
import Foundation
import UIKit


// MARK: - module builder
final class MealDeatilModule: ModuleInterface {

    typealias View = MealDeatilViewController
    typealias Presenter = MealDeatilModulePresenter
    typealias Router = MealDeatilModuleRouter
    typealias Interactor = MealDeatilModuleInteractor

    func build(with meal: Meal) -> UIViewController {
        let view = View()
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()
        
        view.title = meal.name
        view.meal = meal

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)
        router.viewController = view
        return view
    }
}
