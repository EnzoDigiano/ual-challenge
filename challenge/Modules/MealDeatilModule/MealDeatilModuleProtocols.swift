//
//  MealDeatilModuleProtocols.swift
//  challenge
//
//  Created by Enzo Digiano on 17/08/2020.
//

import Foundation

// MARK: - router
protocol MealDeatilModuleRouterPresenterInterface: RouterPresenterInterface {

}

// MARK: - interactor
protocol MealDeatilModuleInteractorPresenterInterface: InteractorPresenterInterface {

}

// MARK: - presenter
protocol MealDeatilModulePresenterRouterInterface: PresenterRouterInterface {

}

protocol MealDeatilModulePresenterInteractorInterface: PresenterInteractorInterface {

}

protocol MealDeatilModulePresenterViewInterface: PresenterViewInterface {

}

// MARK: - view
protocol MealDeatilModuleViewPresenterInterface: ViewPresenterInterface {

}
