//
//  SearchViewController.swift
//  challenge
//
//  Created by Enzo Digiano on 17/08/2020.
//

import UIKit

class SearchViewController: UIViewController, ViewInterface {
    
    var presenter: SearchModulePresenterViewInterface!
    var data: [Meal] = [] { didSet { tableView.reloadData()}}
    let searchController =  UISearchController(searchResultsController: nil)
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bannerImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.startGetRandom()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter.stopGetRandom()
    }
    
    internal func configure(){
        // Configure Navigation Bar
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.searchController = searchController
        navigationItem.largeTitleDisplayMode = .automatic
        navigationItem.hidesSearchBarWhenScrolling = false
        // Configure Search Bar
        searchController.searchBar.delegate = self
        searchController.searchBar.barStyle = .default
        searchController.searchBar.placeholder = "Search"
        // Register Cell
        tableView.register(UINib(nibName: "MealTableViewCell", bundle: nil), forCellReuseIdentifier: "MealCell")
        // Dismiss Keyboard
        self.shouldDismissKeyboard()
    }
}

// MARK: - Presenter
extension SearchViewController: SearchModuleViewPresenterInterface {
    
    func getImage(with path: String) {
        if let imageURL = URL(string: path) {
            bannerImage.kf.setImage(with: .network(imageURL), options: [.transition(.fade(0.2))]) { result in
                switch result {
                case .success( _):
                    print("Sucessfully get image")
                case .failure( _):
                    print("Fail to get image")
                }
            }
        } else {
            print("Fail to make image URL")
        }
    }
}

// MARK: - Table View Delegates
extension SearchViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MealCell", for: indexPath) as! MealTableViewCell
        let meal = data[indexPath.row]
        cell.configure(with: meal)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        searchController.searchBar.resignFirstResponder()
        let meal = data[indexPath.row]
        presenter.startShowDetailScreen(with: meal)
    }
}

// MARK: Search Bar Delegate
extension SearchViewController: UISearchBarDelegate {
    
    internal func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Searching: \(searchText)")
        if searchText.count > 0 {
            presenter.startGetData(with: searchText)
        } else {
            self.data = []
        }
    }
    
    internal func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.data = []
    }
    
    internal func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchController.searchBar.resignFirstResponder()
        if let searchText = searchController.searchBar.text, searchText != "" {
            presenter.startGetData(with: searchText)
        }
    }
}
