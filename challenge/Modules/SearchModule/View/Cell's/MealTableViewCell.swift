//
//  MealTableViewCell.swift
//  challenge
//
//  Created by Enzo Digiano on 17/08/2020.
//

import UIKit
import Kingfisher

class MealTableViewCell: UITableViewCell {

    @IBOutlet weak var mealImage: UIImageView!
    @IBOutlet weak var mealName: UILabel!
    @IBOutlet weak var mealCategory: UILabel!
    
    var meal: Meal? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(with meal: Meal){
        self.meal = meal
        mealName.text = meal.name
        mealCategory.text = meal.category
        getImage(with: meal.pictureURL)
    }
    
    internal func getImage(with path: String) {
        if let imageURL = URL(string: path) {
             mealImage.kf.setImage(with: .network(imageURL), options: [.transition(.fade(0.2))]) { result in
                 switch result {
                     case .success( _):
                         print("Sucessfully get image")
                     case .failure( _):
                         print("Fail to get image")
                 }
             }
         } else {
            print("Fail to make image URL")
         }
    }
}
