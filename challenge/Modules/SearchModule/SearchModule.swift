//
//  SearchModuleModule.swift
//  SearchModule
//
//  Created by Enzo Digiano on 17/8/20.
//
import Foundation
import UIKit


// MARK: - module builder
final class SearchModule: ModuleInterface {

    typealias View = SearchViewController
    typealias Presenter = SearchModulePresenter
    typealias Router = SearchModuleRouter
    typealias Interactor = SearchModuleInteractor

    func build() -> UIViewController {
        let view = View()
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()
        
        view.title = "Meals"

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)
        router.viewController = view
        return view
    }
}
