//
//  SearchModulePresenter.swift
//  SearchModule
//
//  Created by Enzo Digiano on 17/8/20.
//

import Foundation

final class SearchModulePresenter: PresenterInterface {

    internal var timer: Timer?
    internal var timeInterval: Double = 30.0
    var router: SearchModuleRouterPresenterInterface!
    var interactor: SearchModuleInteractorPresenterInterface!
    weak var view: SearchModuleViewPresenterInterface!

}

// MARK: - Router
extension SearchModulePresenter: SearchModulePresenterRouterInterface {

}

// MARK: - Interactor
extension SearchModulePresenter: SearchModulePresenterInteractorInterface {
    
    
    func didGetRandom(with result: Result<[MealsMeal], Error>) {
        switch result {
            case .success(let results):
                guard let meal = results.first?.toMeal() else {
                    print("Can't get meal from result")
                    return
                }
                print("Get random meal: \(meal)")
                self.view.getImage(with: meal.pictureURL)
                break
            case .failure(let error):
                print("Couldn't get random meal: \(error)")
                break
        }
    }
    
    
    func didGetData(with result: Result<[MealsMeal], Error>) {
        switch result {
            case .success(let results):
                print("Get meals: \(results)")
                var meals: [Meal] = []
                for item in results { meals.append(item.toMeal())}
                view.data = meals
                break
            case .failure(let error):
                print("Couldn't get meals: \(error)")
                break
        }
    }
}

// MARK: - View
extension SearchModulePresenter: SearchModulePresenterViewInterface {
    
    func startGetRandom() {
        if timer == nil{
            interactor.makeGetRandom()
            timer = Timer.scheduledTimer(timeInterval: timeInterval,
                                         target: self,
                                         selector: #selector(timerNotifications), userInfo: nil,
                                         repeats: true)
            print("Timer Start")

        }
        
    }
    
    @objc fileprivate func timerNotifications(_ sender: Timer) {
        interactor.makeGetRandom()
    }
    
    func stopGetRandom() {
        timer?.invalidate()
        timer = nil
    }
    
    func startShowDetailScreen(with meal: Meal) {
        router.makeShowDetailScreen(with: meal)
    }
    
    func startGetData(with searchText: String) {
        interactor.makeGetData(with: searchText)
    }
    
}
