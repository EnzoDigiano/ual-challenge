//
//  SearchModuleProtocols.swift
//  challenge
//
//  Created by Enzo Digiano on 17/08/2020.
//

import Foundation

// MARK: - router
protocol SearchModuleRouterPresenterInterface: RouterPresenterInterface {
    func makeShowDetailScreen(with meal: Meal)
}

// MARK: - interactor
protocol SearchModuleInteractorPresenterInterface: InteractorPresenterInterface {
    func makeGetData(with searchText: String)
    func makeGetRandom()
}

// MARK: - presenter
protocol SearchModulePresenterRouterInterface: PresenterRouterInterface {

}

protocol SearchModulePresenterInteractorInterface: PresenterInteractorInterface {
    func didGetData(with result: Result<[MealsMeal], Error>)
    func didGetRandom(with result: Result<[MealsMeal], Error>)
}

protocol SearchModulePresenterViewInterface: PresenterViewInterface {
    var timer: Timer? { get set }
    var timeInterval: Double { get set }
    func startGetData(with searchText: String)
    func startGetRandom()
    func stopGetRandom()
    func startShowDetailScreen(with meal: Meal)
}

// MARK: - view
protocol SearchModuleViewPresenterInterface: ViewPresenterInterface {
    var data: [Meal] { get set }
    func getImage(with path: String) 
}
