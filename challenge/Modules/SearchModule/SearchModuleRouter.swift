//
//  SearchModuleRouter.swift
//  SearchModule
//
//  Created by Enzo Digiano on 17/8/20.
//

import Foundation
import UIKit

final class SearchModuleRouter: RouterInterface {

    weak var presenter: SearchModulePresenterRouterInterface!
    weak var viewController: UIViewController?
}

// MARK: - Presenter
extension SearchModuleRouter: SearchModuleRouterPresenterInterface {
    
    func makeShowDetailScreen(with meal: Meal) {
        let detailViewController = MealDeatilModule().build(with: meal)
        self.viewController?.navigationController?.pushViewController(detailViewController, animated: true)
    }
}
