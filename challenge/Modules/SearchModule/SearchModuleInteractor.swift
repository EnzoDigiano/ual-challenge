//
//  SearchModuleInteractor.swift
//  SearchModule
//
//  Created by Enzo Digiano on 17/8/20.
//

import Foundation
import PromiseKit

final class SearchModuleInteractor: InteractorInterface {

    weak var presenter: SearchModulePresenterInteractorInterface!
}

// MARK: - Presenter
extension SearchModuleInteractor: SearchModuleInteractorPresenterInterface {
    
    func makeGetRandom() {
        let service = MealAPIService()
        firstly {
            service.getRandom()
        }.done { response in
            guard let data = response.meals else {
                print("There be an error parsing data")
                self.presenter.didGetRandom(with: .failure(HTTP_RESPONSE_TYPE.HTTP_FAILURE("Couldn't get meals on response")))
                return
            }
            self.presenter?.didGetRandom(with: .success(data))
        }.catch { error in
            print("There be an error getting songs: \(error.localizedDescription)")
            self.presenter?.didGetRandom(with:.failure(error))
        }
    }
    
    func makeGetData(with searchText: String) {
        let service = MealAPIService()
        firstly {
            service.searchMeals(with: searchText)
        }.done { response in
            guard let data = response.meals else {
                print("There be an error parsing data")
                self.presenter.didGetData(with: .failure(HTTP_RESPONSE_TYPE.HTTP_FAILURE("Couldn't get meals on response")))
                return
            }
            self.presenter?.didGetData(with: .success(data))
        }.catch { error in
            print("There be an error getting songs: \(error.localizedDescription)")
            self.presenter?.didGetData(with:.failure(error))
        }
    }
}
