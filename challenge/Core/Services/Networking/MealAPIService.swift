//
//  MealAPIService.swift
//  challenge
//
//  Created by Enzo Digiano on 17/08/2020.
//

import Foundation
import Alamofire
import PromiseKit

class MealAPIService {
    
    /// Request to API to get a list of`meal` by name
    /// - Parameter id: `String` value
    /// - Returns: `MealsResponse` object or `reject` error
    func searchMeals(with name: String) ->Promise<MealsResponse> {
        let headers: HTTPHeaders = ["Accept": "application/json"]
        
        let search = URLQueryItem(name: "s", value: name)

        var component = URLComponents()
        component.scheme = "https"
        component.host = "www.themealdb.com"
        component.path = "/api/json/v1/1/search.php"
        component.queryItems = [ search ]
        
        return Promise { seal in
            let request = AF.request(component.url!, headers: headers)
            request.responseDecodable(of: MealsResponse.self){ response in
                switch response.result {
                case .success(let value):
                    seal.fulfill(value)
                case .failure(let error):
                    print(error.localizedDescription)
                    seal.reject(error)
                }
            }
        }
    }
    
    /// Request to API to get a `meal` by id
    /// - Parameter id: `String` value
    /// - Returns: `MealsResponse` object or `reject` error
    func lookupMeal(with id: String) ->Promise<MealsResponse> {
        let headers: HTTPHeaders = ["Accept": "application/json"]
        
        let search = URLQueryItem(name: "i", value: id)

        var component = URLComponents()
        component.scheme = "https"
        component.host = "www.themealdb.com"
        component.path = "/api/json/v1/1/lookup.php"
        component.queryItems = [ search ]
        
        return Promise { seal in
            let request = AF.request(component.url!, headers: headers)
            request.responseDecodable(of: MealsResponse.self){ response in
                switch response.result {
                case .success(let value):
                    seal.fulfill(value)
                case .failure(let error):
                    print(error.localizedDescription)
                    seal.reject(error)
                }
            }
        }
    }
    
    /// Request to API to get a `meal` by id
    /// - Returns: `MealsResponse` object or `reject` error
    func getRandom() ->Promise<MealsResponse> {
        let headers: HTTPHeaders = ["Accept": "application/json"]
        
        var component = URLComponents()
        component.scheme = "https"
        component.host = "www.themealdb.com"
        component.path = "/api/json/v1/1/random.php"
        
        return Promise { seal in
            let request = AF.request(component.url!, headers: headers)
            request.responseDecodable(of: MealsResponse.self){ response in
                switch response.result {
                case .success(let value):
                    seal.fulfill(value)
                case .failure(let error):
                    print(error.localizedDescription)
                    seal.reject(error)
                }
            }
        }
    }
}

