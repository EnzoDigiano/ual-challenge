//
//  UIViewController.swift
//  challenge
//
//  Created by Enzo Digiano on 17/08/2020.
//

import Foundation
import UIKit

fileprivate var aView: UIView?

extension UIViewController {
    
    func shouldDismissKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(disdismissKeyboard(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc private func disdismissKeyboard(_ sender: Any) {
        self.view.endEditing(true)
    }
}
